var mongoose = require('mongoose');

var messagesSchema = mongoose.Schema({
  username: String,
  message: String,
  date: Number
});

var Message = mongoose.model('Message', messagesSchema, 'messages');

module.exports = Message;
