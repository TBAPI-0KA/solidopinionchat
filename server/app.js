var options = {
  socketPort: 3333,
  mongoDB: 'mongodb://localhost/mychat'
};

var io = require('socket.io')(options.socketPort);
var mongoose = require('mongoose');

var Message = require('./models/message');

// connect to mongo
mongoose.connect(options.mongoDB);

// catch events
mongoose.connection
  .on('error', onDatabaseError)
  .once('open', onDatabaseSuccess);

// on client connected event
io.on('connection', onClientConnected);

/** This is callback if database have error */
function onDatabaseError() {
  console.log('Database error happened');
}

/** This is callback if database connect successful */
function onDatabaseSuccess() {
  console.log('Database connected successful')
}

/** This is callback if any client connect to server */
function onClientConnected(socket) {

  var login;

  console.log('client connected');

  socket.on('auth', onAuth);
  socket.on('message', onMessageReceived);

  /** This is callback if user send his login */
  function onAuth(data) {
    console.log('client called himself `' + data.login + '`');

    login = data.login;

    sendHistoryForClient();

  }

  /** This is callback if user send any chat message */
  function onMessageReceived(message) {
    console.log('New message received: ' + message);

    var msg = {
      username: login,
      message: message,
      date: new Date().getTime()
    };

    // insert message into db
    (new Message(msg)).save();

    // send message to all clients
    io.sockets.emit('message', msg);
  }

  /** This is func to send chat history to user */
  function sendHistoryForClient() {
    Message.find({}, function (err, docs) {
      socket.emit('history:get', docs);
    });
  }

}

console.log('server started');
