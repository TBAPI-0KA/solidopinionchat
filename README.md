# How to run

## Requirements
Node.JS, NPM, MongoDB, Gulp, Bower

### server run

1. `cd server/`
2. run `npm install`
3. run `node app.js` (do not forget to start MongoDB)

### client side

1. Go to root directory of project
2. run `npm install` 
3. run `bower install`
4. run `gulp build`
5. Configure any http server (like Apache, nginx, express etc.) to serve `dist/` folder. 

### serving built version with the help of `http-server` tool
1. run `npm install -g http-server` 
2. change directory to dist: `cd dist/`
3. run `http-server`
4. go to `localhost:8080` on your browser. Port can be another, http-server display address of site after you run it.