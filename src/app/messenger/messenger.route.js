'use strict';

(function () {

  angular
    .module('messenger')
    .config(Routes);

  /** @ngInject **/
  function Routes($stateProvider) {
    $stateProvider
      .state('messenger', {
        url: '/',
        templateUrl: 'app/messenger/templates/index.html',
        controller: 'MessengerController',
        controllerAs: 'Messenger',
        resolve: {
          authorize: ['User', function (User) {
            return User.authorize();
          }]
        }
      });
  }

})();
