'use strict';

(function () {

  angular
    .module('messenger')
    .service('Messenger', Messenger);

  /** @ngInject **/
  function Messenger() {
    var service = {
      LOGOUT_MESSAGE: '/logout',
      SOCKET_ADDRESS: 'http://localhost:3333'
    };

    return service;

  }

})();
