'use strict';

/*
  This filter help to format message timestamp to HH:MM format
*/

(function () {

  angular
    .module('messenger')
    .filter('chatDate', chatDate);

  /** @ngInject **/
  function chatDate($log) {
    return function (timestamp) {
      try {
        var date = new Date(timestamp);
        return date.getHours().toString().replace( /^([0-9])$/, '0$1' ) + ':' + date.getMinutes().toString().replace( /^([0-9])$/, '0$1' );
      } catch (e) {
        $log.debug('Wrong date for timestamp: ' + timestamp);
      }
    }
  }

})();
