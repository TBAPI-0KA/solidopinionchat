'use strict';

(function () {

  angular
    .module('messenger')
    .controller('MessengerController', MessengerController);

  /** @ngInject **/
  function MessengerController(User, Messenger, socket, $state, $scope) {

    var vm = this;

    vm.messages = [];

    vm.init = init;
    vm.AuthInServer = AuthInServer;
    vm.sendMessage = sendMessage;
    vm.historyLoaded = historyLoaded;
    vm.sendSystemNotice = sendSystemNotice;
    vm.newMessageReceived = newMessageReceived;
    vm.startListenForMessages = startListenForMessages;

    vm.init();

    function init() {
      // system notice to help user how to logout
      vm.sendSystemNotice();
      vm.AuthInServer();
      vm.startListenForMessages();
    }

    function AuthInServer() {
      // say server my username
      socket.emit('auth', {
        login: User.getLogin()
      })
    }

    function newMessageReceived(msg) {
      vm.messages.push(msg);
      $scope.$emit('messenger:newMessage');
    }

    function startListenForMessages() {
      socket.on('message', vm.newMessageReceived);
      socket.on('history:get', vm.historyLoaded);
    }

    function historyLoaded(history) {
      vm.messages = history.concat(vm.messages);
      $scope.$emit('messenger:historyLoaded');
    }

    function sendSystemNotice() {
      vm.messages.push({
        isSystem: true,
        message: 'To logout please type `/logout` into chat'
      });
    }

    function sendMessage() {
      // if user typed /logout we should logout him and redirect to login page
      if (vm.message === Messenger.LOGOUT_MESSAGE) {
        User.logOut();
        $state.go('login');
        return;
      }
      socket.emit('message', vm.message);
      vm.message = '';
    }

  }

})();
