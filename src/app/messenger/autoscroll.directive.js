'use strict';

/*
* This directive make autoscroll to bottom for chat if it has a lot of messages.
*/

(function () {

  angular
    .module('messenger')
    .directive('autoscroll', autoscroll);

  /** @ngInject **/
  function autoscroll($timeout) {
    return {
      restrict: 'A',
      link: function (scope, elem) {
        var $block = elem;
        var $list = $block.find('ul');

        scope.$on('messenger:newMessage', onNewMessageReceived);
        scope.$on('messenger:historyLoaded', onHistoryLoaded);

        function onHistoryLoaded() {
          $timeout(function () {
            // move scroll to bottom
            $block.scrollTop($list.innerHeight());
          }, 1);
        }

        function onNewMessageReceived() {
          $timeout(function () {

            var scrollPosition;
            var maxScroll;

            function isScrollOnBottom() {
              var SCROLL_AROUND = 40;
              //scroll to bottom if scroll is around 40px near bottom
              return maxScroll - SCROLL_AROUND < scrollPosition && maxScroll + SCROLL_AROUND > scrollPosition;
            }

            var BLOCK_PADDING = 20;

            scrollPosition = $block.scrollTop();
            // try to set max height because cannot calculate
            // max scroll by heights - its different in different OS (Mac, Win)
            maxScroll = $block.scrollTop($list.innerHeight()) && $block.scrollTop() - BLOCK_PADDING;

            // move scroll back
            $block.scrollTop(scrollPosition);

            if (isScrollOnBottom()) {
              $block.scrollTop($list.innerHeight());
            }

          }, 1);
        }
      }
    }
  }

})();
