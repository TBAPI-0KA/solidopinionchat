'use strict';

(function () {

  angular
    .module('messenger')
    .service('socket', socket);

  /** @ngInject **/
  function socket($rootScope, Messenger) {
    var socket = io.connect(Messenger.SOCKET_ADDRESS);
    return {
      on: function (eventName, callback) {
        socket.on(eventName, function () {
          var args = arguments;
          // we use $rootScope.$apply if we want to update any
          // scope option and make it updated
          $rootScope.$apply(function () {
            callback.apply(socket, args);
          });
        });
      },
      emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        })
      }
    };
  }

})();
