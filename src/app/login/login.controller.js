'use strict';

(function () {

  angular
    .module('login')
    .controller('LoginController', LoginController);

  /** @ngInject **/
  function LoginController(User, $state, $log) {
    var vm = this;

    vm.logIn = logIn;

    function logIn() {
      $log.debug('User login via username ' + vm.userLogin);
      User.logIn(vm.userLogin);
      $state.go('messenger');
    }

  }

})();
