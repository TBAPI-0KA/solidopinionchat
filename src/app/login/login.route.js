'use strict';

(function () {

  angular
    .module('login')
    .config(Routes);

  /** @ngInject **/
  function Routes($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/templates/index.html',
        controller: 'LoginController',
        controllerAs: 'Login'
      });
  }

})();
