(function () {
  'use strict';

  angular
    .module('chat', ['core', 'messenger', 'login', 'user']);

})();
