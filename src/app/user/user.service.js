'use strict';

(function () {

  angular
    .module('user')
    .service('User', User);

  /** @ngInject **/
  function User($q, $state, $timeout) {

    var service = {
      user: false,
      logIn: logIn,
      getLogin: getLogin,
      logOut: logOut,
      isUserLoggedIn: isUserLoggedIn,
      authorize: authorize
    };

    return service;

    function isUserLoggedIn() {
      return !!service.user;
    }

    function getLogin() {
      return service.user;
    }

    function logIn(user) {
      service.user = user;
    }

    function logOut() {
      service.user = false;
    }

    function authorize() {
      // this func is used on route's resolve to check if user authorized
      // and if not redirect him to login page
      var deferred = $q.defer();

      if (service.isUserLoggedIn()) {
        deferred.resolve();
      } else {
        $timeout(function () {
          $state.go('login');
        }, 1);
        deferred.reject();
      }

      return deferred.promise;

    }


  }

})();
